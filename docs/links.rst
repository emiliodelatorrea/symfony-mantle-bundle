###############
Reference Links
###############

The below table summarizes the relevant links needed to understand the fundamentals
and get started utalizing this project.

:Project Name:  scribe/mantle-bundle
:Release:       |version|
:Copyright:     2014–2015 `Scribe Inc. <https://scribe.software/>`_ All rights reserved.
:License:       `The MIT License (MIT) <https://symfony-mantle-bundle.docs.scribe.tools/license>`_
:Git Source:    https://symfony-mantle-bundle.docs.scribe.tools/git
:Travis CI:     https://symfony-mantle-bundle.docs.scribe.tools/ci
:Code Coverage: https://symfony-mantle-bundle.docs.scribe.tools/coverage
:Code Quality:  https://symfony-mantle-bundle.docs.scribe.tools/quality
:Dependencies:  https://symfony-mantle-bundle.docs.scribe.tools/deps
:API Reference: https://symfony-mantle-bundle.docs.scribe.tools/api
:Documentation: https://symfony-mantle-bundle.docs.scribe.tools/docs
:Packagist:     https://symfony-mantle-bundle.docs.scribe.tools/pkg/mantle-bundle
:Keywords:      symfony2, mantle, foundation, shared, scribe

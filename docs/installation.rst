############
Installation
############

Using Composer
==============

To include this bundle in your project, simply add it as a dependency to your :code:`composer.json` file within the :code:`require` block.

.. code-block:: json

   "require" : {
     "scribe/mantle-bundle" : "dev-master"
   }


After adding Scribe's Mantle Bundle as a dependency, simply run composer to update your vendor files and composer auto-loader includes.

.. code-block:: bash

   composer.phar update

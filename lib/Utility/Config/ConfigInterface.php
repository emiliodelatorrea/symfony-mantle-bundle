<?php

/*
 * This file is part of the Scribe Mantle Bundle.
 *
 * (c) Scribe Inc. <source@scribe.software>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Scribe\Utility\Config;

/**
 * Class ConfigInterface.
 */
interface ConfigInterface
{
    /**
     * Getter for config value.
     *
     * @param string $key config key
     *
     * @throws mixed
     */
    public function get($key);
}

/* EOF */

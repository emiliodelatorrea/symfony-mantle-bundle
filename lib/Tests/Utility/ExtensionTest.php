<?php

/*
 * This file is part of the Scribe Mantle Bundle.
 *
 * (c) Scribe Inc. <source@scribe.software>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Scribe\Tests\Utility;

use Scribe\Utility\UnitTest\AbstractMantleTestCase;
use Scribe\Utility\Extension;

class ExtensionTest extends AbstractMantleTestCase
{
    public function testThrowsExceptionOnInstantiation()
    {
        $this->setExpectedException(
            'Scribe\Exception\RuntimeException',
            'Cannot instantiate static class Scribe\Utility\Extension'
        );

        new Extension();
    }

    public function testAreAnyEnabledSuccess()
    {
        static::assertEquals(
            'memcached',
            Extension::areAnyEnabled('not-real-extension', 'memcached', 'mbstring')
        );
    }

    public function testAreAnyEnabledFailure()
    {
        static::assertFalse(
            Extension::areAnyEnabled('not-real-extension-1', 'not-real-extension-1', 'not-real-extension-1')
        );
    }

    public function testAreAllEnabledSuccess()
    {
        static::assertTrue(
            Extension::areAllEnabled('memcached', 'igbinary', 'twig')
        );
    }

    public function testAreAllEnabledFailure()
    {
        static::assertFalse(
            Extension::areAllEnabled('memcached', 'igbinary', 'twig', 'mongo', 'pdo_mysql', 'mysql', 'not-real')
        );
    }

    public function testIgbinaryIsEnabled()
    {
        static::assertTrue(Extension::hasIgbinary());
    }

    public function testJsonIsEnabled()
    {
        static::assertTrue(Extension::hasJson());
    }

    public function testReflectIsEnabled()
    {
        static::assertTrue(Extension::isEnabled('Reflection'));
    }

    public function testUnknownExtensionIsNotEnabled()
    {
        static::assertFalse(Extension::isEnabled('this-extension-does-not-exist'));
    }

    public function testExceptionOnEmptyString()
    {
        $this->setExpectedException(
            'Scribe\Exception\RuntimeException',
            'Cannot check extension availability against empty string in Scribe\Utility\Extension.'
        );

        Extension::isEnabled('');
    }
}

/* EOF */
